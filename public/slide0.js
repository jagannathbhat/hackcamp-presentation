;(function() {
	let currentAction = 0
	let currentSlide = 0
	let p = 0

	let nextAction = () => {
		if (currentAction < actions.length) actions[currentAction++]()
	}

	let nextSlide = () => {
		document
			.getElementById('slide' + ++currentSlide)
			.classList.add('slide--done')
		setTimeout(() => {
			document.body.style.transform = 'translateY(-' + currentSlide * 100 + '%)'
		}, 300)
	}

	let slideShow = () => {
		document.getElementById('pic' + p).classList.remove('open')
		p = (p + 1) % 4
		setTimeout(
			() => document.getElementById('pic' + p).classList.add('open'),
			400
		)
		setTimeout(slideShow, 5400)
	}

	let slide1action1 = () => {
		document.getElementById('bannerhackclub').classList.add('close')
		document.getElementById('bannermlh').classList.add('close')
		document.getElementById('logo').classList.add('close')
		setTimeout(nextSlide, 1000)
		setTimeout(
			() => document.getElementById('pic0').classList.add('open'),
			2000
		)
		setTimeout(slideShow, 7000)
	}

	let actions = [
		slide1action1,
		nextSlide,
		nextSlide,
		nextSlide,
		nextSlide,
		nextSlide,
		nextSlide
	]

	document.addEventListener('keydown', event => {
		if (event.isComposing || event.keyCode === 229) return
		if (event.keyCode === 39 || event.keyCode === 34) nextAction()
	})

	document.addEventListener('click', nextAction)
})()
